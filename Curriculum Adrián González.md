# Adrián González Borja 
![](https://gitlab.com/39446104t/adriangonzalez/raw/master/1.png)


649603465    
adrian.gonzalezborja2001@gmail.com      
## Objetivo Profesional:
He estado trabajando en el negocio de el padre de un amigo mío reparando ordenadores y varios electrodomésticos, pero no solo estoy ansioso de trabajar en vuestra empresa, si no que también tengo ganas de obtener experiencia laboral, desenvolverme en un entorno nuevo con gente nueva y además trabajar de algo que me apasiona.
## Formación Académica:
- Título de la ESO, Junio de 2017 en el centro Jesús Maria Claudina Thévenet en Barcelona, España.
Cursando actualmente:
- Ciclo Formativo de Grado Medio de Sistemas Microinformáticos y redes.
## Idiomas:
| Catalán        	|     Español    	|    Inglés 	|
|----------------	|:--------------:	|----------:	|
| Lengua materna 	| Lengua paterna 	| Título B2 	|
## Áreas de Experiencia:
- Atención al Público:
Solucioné problemas a los clientes que presentaban sus quejas.
Reparé y trastee con diversos electrodomésticos y ordenadores en un pequeño trabajo de verano en el negocio de le padre de un amigo.
Atendí telefonica y fisicamente a los clientes y orienté sus dudas en función de las necesidades.
- Comunicación:
Hablé con amigos y familiares para ayudarles hablándoles de manera fluida, pero de manera que lo entendieran siendo más o menos técnico dependiendo del nivel de conocimiento en el tema.
## Informática:
- Instalación:
Instalé varios sistemas operativos (como todas las ramifificaciones de Windows, varias variaciones de Linux como Debian y Ubuntu) en ordenadores.
- Ofimática:
Domino programas de edición de imagen y de video.
También domino todas las variaciones de la saga LibreOffice, OpenOffice y Microsoft Office.
## Relaciones Públicas:
- Ayudé en el negocio de el padre de mi amigo.
